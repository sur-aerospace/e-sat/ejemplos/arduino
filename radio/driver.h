#include <Arduino.h>
#include <SPI.h>

extern SPIClass SPI_2;

const int Radio_CS1 = PA9;
const int Radio_CS2 = PA8;
const int Radio_SDN = PB10;
const int Radio_IRQ = PB11;
const int Radio_CTS = PB1;
const int Buzzer = PA15;
const int LED = PC13;

void Radio_ChipSelect_Enable(void);
void Radio_ChipSelect_Disable(void);
uint8_t Radio_SendRecv_Byte(uint8_t data);
void Radio_Command(uint8_t Command, uint8_t *Buffer, size_t NumBytes);
void Radio_Power(uint8_t State);

//*************************************************************************************//

#define Flash_SendRecv_Byte Radio_SendRecv_Byte

void Flash_ChipSelect_Enable(void);
void Flash_ChipSelect_Disable(void);
void Flash_Command(uint8_t Command, uint8_t *Address, size_t AddrSize , uint8_t *Buffer, size_t NumBytes);

