#include <si4463.h>
#include <w25q.h>
#include "fsk/radio_config_Si4463_10kbps_100mw.h"
#include "driver.h"

//*************************************************************************************//

SI4463_HandleTypeDef hradio = {.HAL = {&Radio_Command, &Radio_Power}};
W25Q_HandleTypeDef hstorate = {.HAL = {&Flash_Command}};
uint8_t radio_config[] = RADIO_CONFIGURATION_DATA_ARRAY;

//*************************************************************************************//

uint8_t intStatus[9];
uint8_t count;
uint8_t channel = 0x00;

void setup() {
  pinMode(Radio_CS1, OUTPUT);
  pinMode(Radio_CS2, OUTPUT);
  pinMode(Radio_SDN, OUTPUT);
  pinMode(Radio_CTS, INPUT);
  pinMode(LED, OUTPUT);
  pinMode(Buzzer, OUTPUT);
  SPI_2.begin();
  SPI_2.setBitOrder(MSBFIRST);
  SPI_2.setDataMode(SPI_MODE0);
  SPI_2.setClockDivider(SPI_CLOCK_DIV16);

  Radio_ChipSelect_Disable();
  Flash_ChipSelect_Disable();
  SI4463_PowerDisable(&hradio);
  delay(10);
  SI4463_PowerEnable(&hradio);
  delay(10);
  SI4463_RadioConfig(&hradio, radio_config);
  delay(10);
}

void loop() {
  //Control del LED y Buzzer
  digitalWrite(LED, !digitalRead(LED));
  digitalWrite(Buzzer, !digitalRead(Buzzer));
  
  //Crea paquete en memoria
  uint8_t data[4] = {0x01, 0x02, 0x03, count++};
  
  //Recupera direccion y tamaño del paquete en memoria
  uint8_t *packetAddress = data;
  size_t packetSize = sizeof(data);

  //Control de la radio
  //Prepara radio para transmision
  SI4463_FifoInfo(&hradio, NULL, 0x03);
  SI4463_IntStatus(&hradio, NULL);
  //Copia paquete a buffer (fifo) de la radio 
  SI4463_WriteTxFifo(&hradio, packetAddress, packetSize);
  //Inicia transmision
  SI4463_StartTX(&hradio, channel, packetSize);
  //Espera señal de interrupcion, indica fin de la transmision
  uint32_t timeout = 0x0FFFF;
  while ((timeout--) && (digitalRead(Radio_IRQ)))
  {
    delay(1);
  }
  SI4463_IntStatus(&hradio, intStatus);
  delay(1000);
}
