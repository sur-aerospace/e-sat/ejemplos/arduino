#include "driver.h"

SPIClass SPI_2(2);

void Radio_ChipSelect_Enable(void)
{
  digitalWrite(Radio_CS1, 0);
}

void Radio_ChipSelect_Disable(void)
{
  digitalWrite(Radio_CS1, 1);
}

uint8_t Radio_SendRecv_Byte(uint8_t data)
{
  return SPI_2.transfer(data);
}

void Radio_Command(uint8_t Command, uint8_t *Buffer, size_t NumBytes)
{
  uint32_t timeout = 0xFFFFF;
  while ((timeout--) && (digitalRead(Radio_CTS) == 0));
  Radio_ChipSelect_Enable();
  Radio_SendRecv_Byte(Command);
  for (size_t i = 0; i < NumBytes; i++)
  {
    Buffer[i] = Radio_SendRecv_Byte(Buffer[i]);
  }
  Radio_ChipSelect_Disable();
}

void Radio_Power(uint8_t State)
{
  digitalWrite(Radio_SDN, !State);
}

//*************************************************************************************//

#define Flash_SendRecv_Byte Radio_SendRecv_Byte

void Flash_ChipSelect_Enable(void)
{
  digitalWrite(Radio_CS2, 0);
}

void Flash_ChipSelect_Disable(void)
{
  digitalWrite(Radio_CS2, 1);
}

void Flash_Command(uint8_t Command, uint8_t *Address, size_t AddrSize , uint8_t *Buffer, size_t NumBytes)
{
  Flash_ChipSelect_Enable();
  Flash_SendRecv_Byte(Command);
  for (size_t i = 0; i < AddrSize; i++)
  {
    Flash_SendRecv_Byte(Address[i]);
  }
  for (size_t i = 0; i < NumBytes; i++)
  {
    Buffer[i] = Flash_SendRecv_Byte(Buffer[i]);
  }
  Flash_ChipSelect_Disable();
}

